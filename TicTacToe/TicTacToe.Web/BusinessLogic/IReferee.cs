﻿using System.Collections.Generic;
using TicTacToe.Web.Models;

namespace TicTacToe.Web.BusinessLogic
{
    public interface IReferee
    {
        bool IsOver(Board board, out Symbol winning);
    }
}