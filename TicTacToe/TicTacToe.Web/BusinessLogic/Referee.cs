﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TicTacToe.Web.Models;

namespace TicTacToe.Web.BusinessLogic
{
    public class Referee : IReferee
    {
        public bool IsOver(Board board, out Symbol winning)
        {
            winning = Symbol.none;

            if (board.Cells[0].All(x => x == Symbol.O) || board.Cells[0].All(x => x == Symbol.X))
            {
                winning = board.Cells[0][0];
                return true;
            }
            else if (board.Cells[1].All(x => x == Symbol.O) || board.Cells[1].All(x => x == Symbol.X))
            {
                winning = board.Cells[1][0];
                return true;
            }
            else if (board.Cells[2].All(x => x == Symbol.O) || board.Cells[2].All(x => x == Symbol.X))
            {
                winning = board.Cells[2][0];
                return true;
            }
            else if (board.Cells[0][0] == board.Cells[1][0] && board.Cells[1][0] == board.Cells[2][0] && board.Cells[0][0] != Symbol.none)
            {
                winning = board.Cells[0][0];
                return true;
            }
            else if (board.Cells[0][1] == board.Cells[1][1] && board.Cells[1][1] == board.Cells[2][1] && board.Cells[0][1] != Symbol.none)
            {
                winning = board.Cells[0][1];
                return true;
            }
            else if (board.Cells[0][2] == board.Cells[1][2] && board.Cells[1][2] == board.Cells[2][2] && board.Cells[0][2] != Symbol.none)
            {
                winning = board.Cells[0][2];
                return true;
            }
            else if (board.Cells[0][0] == board.Cells[1][1] && board.Cells[1][1] == board.Cells[2][2] && board.Cells[0][0] != Symbol.none)
            {
                winning = board.Cells[0][0];
                return true;
            }
            else if (board.Cells[0][2] == board.Cells[1][1] && board.Cells[1][1] == board.Cells[2][0] && board.Cells[0][2] != Symbol.none)
            {
                winning = board.Cells[0][2];
                return true;
            }
            else if (board.Cells[0].All(x => x != Symbol.none) && board.Cells[1].All(x => x != Symbol.none) && board.Cells[2].All(x => x != Symbol.none))
            {
                winning = Symbol.none;
                return true;
            }

            return false;
        }
    }
}