﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicTacToe.Web.BusinessLogic;
using TicTacToe.Web.Models;

namespace TicTacToe.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IReferee _referee;

        public HomeController(IReferee referee)
        {
            _referee = referee;
        }
        public ActionResult Index()
        {
            return View(new IndexModel());
        }

        [HttpPost]
        public ActionResult Index(IndexModel model)
        {
            ModelState.Clear();
            model.NextPlayer = model.NextPlayer == Symbol.X ? Symbol.O : Symbol.X;

            Symbol winning;
            if (!_referee.IsOver(model.Board, out winning))
                return PartialView("Board", model);
            else if (_referee.IsOver(model.Board, out winning) && winning == Symbol.none)
            {
                return PartialView("Draw");
            }
            else
            {
                return PartialView("Over", winning);
            }
        }
    }
}