﻿namespace TicTacToe.Web.Models
{
    public enum Symbol
    {
        none = 0,
        O = 1,
        X = 2
    }
}