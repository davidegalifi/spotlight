﻿using System.Collections.Generic;

namespace TicTacToe.Web.Models
{
    public class Board
    {
        public List<List<Symbol>> Cells { get; set; }

        public Board()
        {
            Cells = new List<List<Symbol>>();
            Cells.Add(new List<Symbol>() { Symbol.none, Symbol.none, Symbol.none });
            Cells.Add(new List<Symbol>() { Symbol.none, Symbol.none, Symbol.none });
            Cells.Add(new List<Symbol>() { Symbol.none, Symbol.none, Symbol.none });
        }
    }
}