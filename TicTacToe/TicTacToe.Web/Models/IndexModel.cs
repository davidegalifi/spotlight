﻿namespace TicTacToe.Web.Models
{
    public class IndexModel
    {
        public Symbol NextPlayer { get; set; }

        public Board Board { get; set; }

        public IndexModel()
        {
            Board = new Board();
            NextPlayer = Symbol.X;
        }
    }
}
