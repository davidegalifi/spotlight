﻿using Moq;
using NUnit.Framework;
using System.Web.Mvc;
using TicTacToe.Web.BusinessLogic;
using TicTacToe.Web.Controllers;
using TicTacToe.Web.Models;

namespace TicTacToe.Test
{
    [TestFixture]
    public class ControllerTests
    {
        private HomeController _controller;
        private Mock<IReferee> _referee;

        [SetUp]
        public void Setup()
        {
            _referee = new Mock<IReferee>();
            _controller = new HomeController(_referee.Object);
        }

        [Test]
        public void When_Get_Index_Return_HomePage()
        {
            var ret = _controller.Index();

            Assert.IsTrue(ret.GetType() == typeof(ViewResult));
            Assert.IsTrue((ret as ViewResult).Model.GetType() == typeof(IndexModel));
        }

        [Test]
        public void When_X_Wins()
        {
            //setup
            Symbol s = Symbol.X;
            _referee.Setup(x => x.IsOver(It.IsAny<Board>(), out s)).Returns(true);

            var ret = _controller.Index(new IndexModel());

            Assert.IsTrue(ret.GetType() == typeof(PartialViewResult));
            Assert.AreEqual("Over", (ret as PartialViewResult).ViewName);
            Assert.IsTrue((ret as PartialViewResult).Model.GetType() == typeof(Symbol));
            Assert.AreEqual((Symbol)(ret as PartialViewResult).Model, Symbol.X);
        }

        [Test]
        public void When_O_Wins()
        {
            //setup
            Symbol s = Symbol.O;
            _referee.Setup(x => x.IsOver(It.IsAny<Board>(), out s)).Returns(true);

            var ret = _controller.Index(new IndexModel());

            Assert.IsTrue(ret.GetType() == typeof(PartialViewResult));
            Assert.AreEqual("Over", (ret as PartialViewResult).ViewName);
            Assert.IsTrue((ret as PartialViewResult).Model.GetType() == typeof(Symbol));
            Assert.AreEqual((Symbol)(ret as PartialViewResult).Model, Symbol.O);
        }


        [Test]
        public void When_Not_Over()
        {
            //setup
            Symbol s = Symbol.O;
            _referee.Setup(x => x.IsOver(It.IsAny<Board>(), out s)).Returns(false);

            var ret = _controller.Index(new IndexModel());

            Assert.IsTrue(ret.GetType() == typeof(PartialViewResult));
            Assert.AreEqual("Board", (ret as PartialViewResult).ViewName);
            Assert.IsTrue((ret as PartialViewResult).Model.GetType() == typeof(IndexModel));
        }

        [Test]
        public void When_Draw()
        {
            //setup
            Symbol s = Symbol.none;
            _referee.Setup(x => x.IsOver(It.IsAny<Board>(), out s)).Returns(true);

            var ret = _controller.Index(new IndexModel());

            Assert.IsTrue(ret.GetType() == typeof(PartialViewResult));
            Assert.AreEqual("Draw", (ret as PartialViewResult).ViewName);
        }
    }
}
