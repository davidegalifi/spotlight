﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Web.BusinessLogic;
using TicTacToe.Web.Models;

namespace TicTacToe.Test
{
    [TestFixture]
    public class RefereeTests
    {
        private IReferee _referee;

        [SetUp]
        public void Setup()
        {
            _referee = new Referee();
        }

        [TestCase(0, Symbol.X)]
        [TestCase(1, Symbol.X)]
        [TestCase(2, Symbol.X)]
        [TestCase(0, Symbol.O)]
        [TestCase(1, Symbol.O)]
        [TestCase(2, Symbol.O)]
        public void When_X_row_has_all_same_symbols_Then_The_Symbol_wins(int row, Symbol s)
        {
            var board = GetBoardWithRowX(row, s);

            var res = _referee.IsOver(board, out var sym);

            Assert.IsTrue(res);
            Assert.AreEqual(sym, s);
        }

        [TestCase(0, Symbol.X)]
        [TestCase(1, Symbol.X)]
        [TestCase(2, Symbol.X)]
        [TestCase(0, Symbol.O)]
        [TestCase(1, Symbol.O)]
        [TestCase(2, Symbol.O)]
        public void When_X_column_has_all_same_symbols_Then_The_Symbol_wins(int col, Symbol s)
        {
            var board = GetBoardWithColumnsX(col, s);

            var res = _referee.IsOver(board, out var sym);

            Assert.IsTrue(res);
            Assert.AreEqual(sym, s);
        }

        [TestCase(Symbol.X)]
        [TestCase(Symbol.O)]
        public void When_Left_Diagonal_Wins(Symbol s)
        {
            var board = new Board();
            board.Cells[0][0] = board.Cells[1][1] = board.Cells[2][2] = s;


            var res = _referee.IsOver(board, out var sym);

            Assert.IsTrue(res);
            Assert.AreEqual(sym, s);
        }

        [TestCase(Symbol.X)]
        [TestCase(Symbol.O)]
        public void When_Right_Diagonal_Wins(Symbol s)
        {
            var board = new Board();
            board.Cells[0][2] = board.Cells[1][1] = board.Cells[2][0] = s;


            var res = _referee.IsOver(board, out var sym);

            Assert.IsTrue(res);
            Assert.AreEqual(sym, s);
        }

        [Test]
        public void When_Draw()
        {
            var board = new Board();
            board.Cells[0][1] = board.Cells[1][1] = board.Cells[1][2] = board.Cells[2][0] = board.Cells[2][2] = Symbol.X;

            board.Cells[0][0] = board.Cells[0][2] = board.Cells[1][0] = board.Cells[2][1] = Symbol.O;

            var res = _referee.IsOver(board, out var sym);

            Assert.IsTrue(res);
            Assert.AreEqual(sym, Symbol.none);
        }

        [Test]
        public void WhennotOver()
        {
            var board = new Board();
            board.Cells[0][1] = board.Cells[1][1] = board.Cells[1][2] = board.Cells[2][0] = board.Cells[2][2] = Symbol.X;

            board.Cells[0][0] = board.Cells[0][2] = board.Cells[1][0] =  Symbol.O;

            var res = _referee.IsOver(board, out var sym);

            Assert.IsFalse(res);
            Assert.AreEqual(sym, Symbol.none);
        }


        private Board GetBoardWithRowX(int row, Symbol s)
        {
            var b = new Board();
            b.Cells[row][0] = b.Cells[row][1] = b.Cells[row][2] = s;

            return b;
        }

        private Board GetBoardWithColumnsX(int column, Symbol s)
        {
            var b = new Board();
            b.Cells[0][column] = b.Cells[1][column] = b.Cells[2][column] = s;

            return b;
        }
    }
}
