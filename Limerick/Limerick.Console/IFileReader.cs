﻿namespace Limerick.Console
{
    public interface IFileReader
    {
        string GetFileContent(string filepath);
    }
}
