﻿using System;
using System.Linq;

namespace Limerick.Console
{
    public class Reverser
    {
        private readonly IFileReader _fileReader;

        public Reverser(IFileReader fileReader)
        {
            _fileReader = fileReader;
        }

        public string GetReverseContent(string filepath)
        {
            string fileContent = _fileReader.GetFileContent(filepath);

            var lines = fileContent.Split(Environment.NewLine.ToArray(), StringSplitOptions.RemoveEmptyEntries).ToList();

            lines.Reverse();

            return string.Join(Environment.NewLine, lines);
        }
    }
}
