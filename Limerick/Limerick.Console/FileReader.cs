﻿using System.IO;

namespace Limerick.Console
{
    public class FileReader : IFileReader
    {
        public string GetFileContent(string filepath)
        {
            using (FileStream stream = new FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }
    }
}
