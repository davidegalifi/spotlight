﻿using System.IO;
using System.Text;

namespace Limerick.Console
{
    class Program
    {
        public static void Main(string[] args)
        {
            var container = new SimpleInjector.Container();

            // Registrations here
            container.Register<IFileReader, FileReader>();
            
            var reverser = container.GetInstance<Reverser>();

            string filePath = args[0];
            string fileOutPath = args[1];

            string content = reverser.GetReverseContent(filePath);

            using (FileStream fs = new FileStream(fileOutPath, FileMode.CreateNew, FileAccess.Write, FileShare.None))
            {
                var bytes = Encoding.UTF8.GetBytes(content);
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();
            }
        }
    }
}
