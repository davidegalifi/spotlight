﻿using Limerick.Console;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limerick.Tests
{
    [TestFixture]
    public class ReverserTest
    {
        private Reverser reverser;
        private Mock<IFileReader> _fileReader;

        private string filePath;

        [SetUp]
        public void Initialize()
        {
            _fileReader = new Mock<IFileReader>();
            reverser = new Reverser(_fileReader.Object);

            filePath = "validFilePath";
        }

        [Test]
        public void DoWork_fileEmpty_ReturnsStrEmpty()
        {
            _fileReader.Setup(x => x.GetFileContent(It.IsAny<string>())).Returns(string.Empty);

            Assert.AreEqual(string.Empty, reverser.GetReverseContent(filePath));
        }

        [Test]
        public void DoWork_OneLine_ReturnsSame()
        {
            _fileReader.Setup(x => x.GetFileContent(It.IsAny<string>())).Returns("OneLine");

            Assert.AreEqual("OneLine", reverser.GetReverseContent(filePath));
        }

        [Test]
        public void DoWork_Success()
        {
            _fileReader.Setup(x => x.GetFileContent(It.IsAny<string>())).Returns("OneLine\r\nHello");

            Assert.AreEqual("Hello\r\nOneLine", reverser.GetReverseContent(filePath));
        }
    }
}
