﻿namespace FizzBuzz
{
    public class Worker
    {
        public string[] GetNumbers()
        {
            string[] ret = new string[100];

            for (int i = 0; i < 100; i++)
            {
                if ((i + 1) % 3 == 0 && (i + 1) % 5 == 0)
                    ret[i] = "FizzBuzz";
                else if ((i + 1) % 3 == 0)
                    ret[i] = "Fizz";
                else if ((i + 1) % 5 == 0)
                    ret[i] = "Buzz";
                else
                    ret[i] = (i + 1).ToString();
            }

            return ret;
        }
    }
}
