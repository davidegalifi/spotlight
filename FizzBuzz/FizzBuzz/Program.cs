﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            Worker worker = new Worker();

            foreach (var item in worker.GetNumbers())
            {
                Console.WriteLine(item);
            }

            Console.ReadKey(); 
        }
    }
}
