﻿using NUnit.Framework;

namespace FizzBuzz.Test
{
    [TestFixture]
    public class WorkerTest
    {
        private Worker _worker;
        
        [SetUp]
        public void Setup()
        {
            _worker = new Worker();
        }

        [Test]
        public void When_Call_GetNUmbers_Return_100_Items()
        {
            Assert.AreEqual(100, _worker.GetNumbers().Length);
        }

        [Test]
        public void When_Call_GetNUmbers_First_And_Last_Element_Are_Buzz()
        {
            var array = _worker.GetNumbers();
            Assert.AreEqual(1.ToString(), array[0]);
            Assert.AreEqual("Buzz", array[99]);
        }

        [Test]
        public void When_Call_GetNUmbers_Divisible_by_3_Fizz()
        {
            var array = _worker.GetNumbers();

            Assert.AreEqual("Fizz", array[2]);
            Assert.AreEqual("Fizz", array[8]);
            Assert.AreEqual("Fizz", array[98]);
        }

        [Test]
        public void When_Call_GetNUmbers_Divisible_by_5_Buzz()
        {
            var array = _worker.GetNumbers();

            Assert.AreEqual("Buzz", array[4]);
            Assert.AreEqual("Buzz", array[9]);
            Assert.AreEqual("Buzz", array[24]);
        }

        [Test]
        public void When_Call_GetNUmbers_Divisible_by_3_and_5_FizzBuzz()
        {
            var array = _worker.GetNumbers();

            Assert.AreEqual("FizzBuzz", array[14]);
            Assert.AreEqual("FizzBuzz", array[29]);
            Assert.AreEqual("FizzBuzz", array[59]);
        }
    }
}
